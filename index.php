<?php require_once 'base.php' ?>
<html ng-app>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php startblock('addheadscript') ?>
    <link rel="stylesheet" href="./static/bootstrap/css/landing-page.css"  />
    <link rel="stylesheet" href="./static/bootstrap/css/carousel.css"  />
    <?php endblock() ?>
<head>
<title>Home | Mealsonwheels</title>
</head>
<body>

<?php startblock('main') ?>
<hr>
<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="./static/bootstrap/img/SM02-2_NorthIndianThali1.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <!-- <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> -->
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="./static/bootstrap/img/thali-1030x778.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <!-- <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="./static/bootstrap/img/Vegetarian_Curry.jpeg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <!-- <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p> -->
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-rounded" src="./static/bootstrap/img/siem_reap_restaurants_indian_maharajah_fish_thalis.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Deluxe Thali &#x20B9; 100 /- </h2>
          <p><a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseDeluxeExample" aria-expanded="false" aria-controls="collapseDeluxeExample">View details &raquo;</a></p>
          
          <!-- collapse content -->
          <div class="collapse" id="collapseDeluxeExample">
            <div class="well">
              <ul>
                Dal<br>
                Vegetable - 1<br>
                Rice<br>
                Chapati - 5<br>
                Raita<br>
                Sweet - 1<br>
                Salad<br>

              </ul>
            </div>
          </div>


        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-rounded" src="./static/bootstrap/img/1339159768220.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Maharaja Thali &#x20B9; 120 /- </h2>
          
          <p><a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseMaharajaExample" aria-expanded="false" aria-controls="collapseMaharajaExample">View details &raquo;</a></p>
          <!-- collapse content -->
          <div class="collapse" id="collapseMaharajaExample">
            <div class="well">
              <ul>
                Dal<br>
                Vegetable - 1<br>
                Paneer Vegetable - 1<br>
                Rice<br>
                Chapati - 5<br>
                Raita<br>
                Sweet - 1<br>
                Salad<br>
                Pickle<br>
                Papad

              </ul>
            </div>
          </div>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-rounded" src="./static/bootstrap/img/09.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Special Malwa Thali &#x20B9; 120 /- </h2>
          <p><a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseSpecialMalwaExample" aria-expanded="false" aria-controls="collapseSpecialMalwaExample">View details &raquo;</a></p>
          <!-- collapse content -->
          <div class="collapse" id="collapseSpecialMalwaExample">
            <div class="well">
              <ul>
                Dal<br>
                Bafle - 4<br>
                Laddu<br>
                Special Chatni<br>
                Salad<br>
                Pickle<br>
                Papad

              </ul>
            </div>
          </div>
        </div><!-- /.col-lg-4 -->
        

      </div><!-- /.row -->

      <div class="row">
        
        <div class="col-lg-4">
          <img class="img-rounded" src="./static/bootstrap/img/non_veg_thali.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Non-Veg Thali &#x20B9; 120 /- </h2>
          
          <p><a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseNonVegExample" aria-expanded="false" aria-controls="collapseNonVegExample">View details &raquo;</a></p>
          <!-- collapse content -->
          <div class="collapse" id="collapseNonVegExample">
            <div class="well">
              <ul>
                Choice of Chicken/Mutton<br>
                Chapati - 5<br>
                Rice<br>
                Salad<br>
                Pickle<br>
                Papad

              </ul>
            </div>
          </div>


        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-rounded" src="./static/bootstrap/img/egg_curry.jpg" alt="Generic placeholder image" width="140" height="140">
          <h2>Egg Thali &#x20B9; 120 /- </h2>
          
          <p><a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseEggExample" aria-expanded="false" aria-controls="collapseEggExample">View details &raquo;</a></p>

          <!-- collapse content -->
          <div class="collapse" id="collapseEggExample">
            <div class="well">
              <ul>
                Choice of Egg Curry/Egg Bhurji<br>
                Chapati - 5<br>
                Rice<br>
                Salad<br>
                Pickle<br>
                Papad

              </ul>
            </div>
          </div>



        </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
      </div>          

      </div><!-- /.row -->


      


<?php endblock() ?>



<?php startblock('addtailscript') ?>

<script type="text/javascript">
$('.carousel').carousel({
  interval: 2000
})
</script>
<?php endblock() ?>
</body>
</html>
