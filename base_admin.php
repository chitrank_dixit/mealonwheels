<?php 
require './components/ti.php';
require './components/core.inc.php';
?>
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<head>
<?php startblock('headscript') ?>
<link rel="stylesheet" href="./static/bootstrap/css/bootstrap.min.css"  />
<link rel="stylesheet" href="./static/bootstrap/css/sb-admin.css" >
<!-- Morris Charts CSS -->
<link rel="stylesheet" href="./static/bootstrap/css/plugins/morris.css" >
<link rel="stylesheet" href="./static/bootstrap/font-awesome/css/font-awesome.min.css">
<!-- <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
<?php endblock() ?>

<?php emptyblock('addheadscript') ?>

</head>

<body>
<!-- header -->
<?php startblock('header') ?>
	<?php include 'components/admin_nav.php' ?>
<?php endblock() ?>

<!-- content here -->
<div id='main'>
  <?php emptyblock('main') ?>
</div>

<!-- footer -->
<?php startblock('footer') ?>
<?php include 'components/footer.php' ?>
<?php endblock() ?>

<?php startblock('tailscript') ?>
<script type="text/javascript" src="./static/bootstrap/js/jquery-2.1.1.js" > </script>
<script type="text/javascript" src="./static/bootstrap/js/bootstrap3.js" > </script>
<script type="text/javascript" src="./static/bootstrap/js/morris/raphael.min.js"></script>
<script type="text/javascript" src="./static/bootstrap/js/morris/morris.min.js"></script>
<script type="text/javascript" src="./static/bootstrap/js/morris/morris-data.js"></script>
<!-- <script type="text/javascript" src="./static/bootstrap/js/angular.js" > </script> -->
<!-- <script type="text/javascript" src="./static/bootstrap/js/jquery.validate.js" > </script> -->
<!-- <script type="text/javascript" src="./static/bootstrap/js/additional-methods.js" > </script> -->
<script type="text/javascript">
$(document).ready(function () {



/*$('#signin_form').validate({
    rules: {
        
        
    },
    
    highlight: function (element) {
        $(element).closest('control-group').removeClass('success').addClass('error');
    },
    success: function (element) {
        element.text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
    }
});

$('#signup_form').validate({
    rules: {
        
        
    },
    
    highlight: function (element) {
        $(element).closest('control-group').removeClass('success').addClass('error');
    },
    success: function (element) {
        element.text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
    }
});*/


});

</script>
<?php endblock() ?>

<?php emptyblock('addtailscript') ?>
</body>
</html>
