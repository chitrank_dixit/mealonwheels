<?php 
require './components/ti.php';
require './components/core.inc.php';
?>
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<head>
<?php startblock('headscript') ?>
<link rel="stylesheet" href="./static/bootstrap/css/bootstrap.min.css"  />
<link href="./static/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
<?php endblock() ?>

<?php emptyblock('addheadscript') ?>

</head>

<body>
<!-- header -->
<?php startblock('header') ?>
	
<?php endblock() ?>

<!-- content here -->
<div id='main'>
  <?php startblock('main') ?>
  <div class="row">
  	<div class="col-sm-4"></div>
  	<div class="col-sm-4">Mealonwheels Admin Panel</div>
  	<div class="col-sm-4"></div>

  </div>
  <div class="row">
  	<div class="col-sm-4"></div>

  <div class="col-sm-4">
  <form class="form-group" name="admin_signin" method="post" action="./db_store/adminsignin_db.php" >
  	Username: <input class="form-control" type="text" name="ausername" id="ausername" /> <br><br>
  	Password: <input class="form-control" type="password" name="apassword" id="apassword" /> <br><br>
  	<input class="form-control btn btn-primary" type="submit" name="Sign In" id="Sign In" value="Sign In" />
  </form>
  </div>
  	<div class="col-sm-4"></div>
  </div>
  <?php endblock() ?>
</div>

<!-- footer -->
<?php startblock('footer') ?>
<?php include 'components/footer.php' ?>
<?php endblock() ?>

<?php startblock('tailscript') ?>
<script type="text/javascript" src="./static/bootstrap/js/jquery-2.1.1.js" > </script>
<script type="text/javascript" src="./static/bootstrap/js/bootstrap3.js" > </script>
<!-- <script type="text/javascript" src="./static/bootstrap/js/angular.js" > </script> -->
<!-- <script type="text/javascript" src="./static/bootstrap/js/jquery.validate.js" > </script> -->
<!-- <script type="text/javascript" src="./static/bootstrap/js/additional-methods.js" > </script> -->
<script type="text/javascript">
$(document).ready(function () {



/*$('#signin_form').validate({
    rules: {
        
        
    },
    
    highlight: function (element) {
        $(element).closest('control-group').removeClass('success').addClass('error');
    },
    success: function (element) {
        element.text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
    }
});

$('#signup_form').validate({
    rules: {
        
        
    },
    
    highlight: function (element) {
        $(element).closest('control-group').removeClass('success').addClass('error');
    },
    success: function (element) {
        element.text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
    }
});*/


});

</script>
<?php endblock() ?>

<?php emptyblock('addtailscript') ?>
</body>
</html>
