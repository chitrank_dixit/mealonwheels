<?php require_once 'base.php';
//require './components/connection.php';
require './db_store/user_orders.php';

?>
<html>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php startblock('addheadscript') ?>
    
    
    
    
    <?php endblock() ?>
<head>
<title><?php echo $_SESSION['username']; ?> Your Orders | Mealsonwheels</title>
</head>
<body>

<?php startblock('main') ?>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container">

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-pills" role="tablist">
    <li role="presentation" class="active"><a href="#active_orders" aria-controls="active_orders" role="tab" data-toggle="tab">Active Orders</a></li>
    <li role="presentation"><a href="#delivered" aria-controls="delivered" role="tab" data-toggle="tab">Delivered</a></li>
    <li role="presentation"><a href="#cancelled" aria-controls="cancelled" role="tab" data-toggle="tab">Cancelled</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="active_orders">
        <?php 
            $i = 0;
            for($i=0;$i<=$_SESSION['num_rows'];$i++)     
            {
                if($_SESSION['user_orders'][$i]['status'] == 'active')
                {
                echo '<br><br>';
                ?>
                <form name="userorders_active" id="userorders_active" action="./db_store/cancel_order.php" method="post">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <p class="pull-right"><?php 
                        $timestamp = explode(" ",$_SESSION['user_orders'][$i]['timestamp']); 
                        $date = $timestamp[0];
                        $date_parts = explode("-", $date);
                        $date = $date_parts[2].'/'.$date_parts[1].'/'.$date_parts[0];
                        $time = $timestamp[1];
                        echo "placed on:   ".$date." ".$time;
                        ?></p>
                        <h3 class="panel-title" id="dis_order_id" name="dis_order_id"><?php echo $_SESSION['user_orders'][$i]['order_id']; ?></h3>
                        <input type="hidden" id="disp_order_id" name="disp_order_id" value="<?php echo $_SESSION['user_orders'][$i]['order_id']; ?>" />
                    </div>
                    <div class="panel-body">
                        <p class="pull-right" id="disp_customername" name="disp_customername"><?php echo $_SESSION['user_orders'][$i]['customername']; ?></p>
                        <p>Customer Name</p>
                        
                        <p class="pull-right" id="disp_mobileno" name="disp_mobileno"><?php echo $_SESSION['user_orders'][$i]['mobileno']; ?></p>
                        <p>Mobile #</p>

                        <p class="pull-right" id="disp_traino" name="disp_traino"><?php echo $_SESSION['user_orders'][$i]['trainno']; ?></p>
                        <p>Train #</p>
                        

                        <p class="pull-right" id="disp_coachno" name="disp_coachno"><?php echo $_SESSION['user_orders'][$i]['coachno']; ?></p>
                        <p>Coach #</p>
                        

                        <p class="pull-right" id="disp_birthno" name="disp_birthno"><?php echo $_SESSION['user_orders'][$i]['birthno']; ?></p>
                        <p>Birth #</p>
                        
                    </div>
                    <div class="panel-footer">
                        <p class="pull-right" id="disp_price" name="disp_price"><?php echo $_SESSION['user_orders'][$i]['price']; ?></p>
                        <p>Price</p>
                        <input type="submit" class="btn btn-danger" value="Cancel Order" />
                    </div>
                </div>
                </form>
                <?php
                //echo "<div class='panel panel-info'> <div class='panel-heading'>"."<h3 class='panel-title'>".$_SESSION['user_orders'][$i]['order_id']."</h3></div>"." <br><div class='panel-body'> "."<p class='pull-right'>".$_SESSION['user_orders'][$i]['customername']."</p><p>Customer Name</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['trainno']."</p><p>Train</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['coachno']."</p><p>Coach #</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['birthno']."</p><p>Birth #</p>"."</div> <div class='panel-footer'>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['price']."</p><p>Price</p>"."</div></div>";
                }
            }
        ?>

    </div>
    <div role="tabpanel" class="tab-pane" id="delivered">
        <?php 
            $i = 0;
            for($i=0;$i<=$_SESSION['num_rows'];$i++)     
            {
                if($_SESSION['user_orders'][$i]['status'] == 'delivered')
                {
                echo '<br><br>';
                ?>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <p class="pull-right"><?php 
                        $timestamp = explode(" ",$_SESSION['user_orders'][$i]['timestamp']); 
                        $date = $timestamp[0];
                        $date_parts = explode("-", $date);
                        $date = $date_parts[2].'/'.$date_parts[1].'/'.$date_parts[0];
                        $time = $timestamp[1];
                        echo "placed on:   ".$date." ".$time;
                        ?></p>
                        <h3 class="panel-title"><?php echo $_SESSION['user_orders'][$i]['order_id']; ?></h3>

                    </div>
                    <div class="panel-body">
                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['customername']; ?></p>
                        <p>Customer Name</p>
                        
                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['mobileno']; ?></p>
                        <p>Mobile #</p>

                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['trainno']; ?></p>
                        <p>Train #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['coachno']; ?></p>
                        <p>Coach #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['birthno']; ?></p>
                        <p>Birth #</p>
                        
                    </div>
                    <div class="panel-footer">
                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['price']; ?></p>
                        <p>Price</p>
                    </div>
                </div>

                <?php
                //echo "<div class='panel panel-success'> <div class='panel-heading'>"."<h3 class='panel-title'>".$_SESSION['user_orders'][$i]['order_id']."</h3></div>"." <br><div class='panel-body'> "."<p class='pull-right'>".$_SESSION['user_orders'][$i]['customername']."</p><p>Customer Name</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['trainno']."</p><p>Train</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['coachno']."</p><p>Coach #</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['birthno']."</p><p>Birth #</p>"."</div> <div class='panel-footer'>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['price']."</p><p>Price</p>"."</div></div>";
                }
            }
        ?>
    </div>
    
    <div role="tabpanel" class="tab-pane" id="cancelled">
        <?php 
            $i = 0;
            for($i=0;$i<=$_SESSION['num_rows'];$i++)     
            {
                if($_SESSION['user_orders'][$i]['status'] == 'cancelled')
                {
                echo '<br><br>';
                ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <p class="pull-right"><?php 
                        $timestamp = explode(" ",$_SESSION['user_orders'][$i]['timestamp']); 
                        $date = $timestamp[0];
                        $date_parts = explode("-", $date);
                        $date = $date_parts[2].'/'.$date_parts[1].'/'.$date_parts[0];
                        $time = $timestamp[1];
                        echo "placed on:   ".$date." ".$time;
                        ?></p>
                        <h3 class="panel-title"><?php echo $_SESSION['user_orders'][$i]['order_id']; ?></h3>

                    </div>
                    <div class="panel-body">
                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['customername']; ?></p>
                        <p>Customer Name</p>
                        
                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['mobileno']; ?></p>
                        <p>Mobile #</p>

                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['trainno']; ?></p>
                        <p>Train #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['coachno']; ?></p>
                        <p>Coach #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['birthno']; ?></p>
                        <p>Birth #</p>
                        
                    </div>
                    <div class="panel-footer">
                        <p class="pull-right"><?php echo $_SESSION['user_orders'][$i]['price']; ?></p>
                        <p>Price</p>
                    </div>
                </div>

                <?php
                //echo "<div class='panel panel-success'> <div class='panel-heading'>"."<h3 class='panel-title'>".$_SESSION['user_orders'][$i]['order_id']."</h3></div>"." <br><div class='panel-body'> "."<p class='pull-right'>".$_SESSION['user_orders'][$i]['customername']."</p><p>Customer Name</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['trainno']."</p><p>Train</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['coachno']."</p><p>Coach #</p>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['birthno']."</p><p>Birth #</p>"."</div> <div class='panel-footer'>"."<p class='pull-right'>".$_SESSION['user_orders'][$i]['price']."</p><p>Price</p>"."</div></div>";
                }
            }
        ?>
    </div>
    

  </div>

</div>

</div>

      


<?php endblock() ?>



<?php startblock('addtailscript') ?>


<script type="text/javascript">
var order_id = document.getElementById('disp_order_id').innerHTML;
htmlWidth = $("html").width();
    $.ajax({
        type: "POST",
        url: "mobileView.php",
        data:{ width: htmlWidth , disp_order_id: order_id }, 
        success: function(data){
            console.log(data); 
        }
    })

</script>
<?php endblock() ?>
</body>
</html>




