<?php require_once 'base_admin.php';
require './db_store/all_orders.php';
?>
<html>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php startblock('addheadscript') ?>
    
      
    
    
    <?php endblock() ?>
<head>
<title>Admin Dashboard | Mealsonwheels</title>
</head>
<body>

<?php startblock('main') ?>

<div id="page-wrapper">
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">


<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-pills" role="tablist">
    <li role="presentation" class="active"><a href="#active_orders" aria-controls="active_orders" role="tab" data-toggle="tab">Active Orders</a></li>
    <li role="presentation"><a href="#delivered" aria-controls="delivered" role="tab" data-toggle="tab">Delivered</a></li>
    <li role="presentation"><a href="#cancelled" aria-controls="cancelled" role="tab" data-toggle="tab">Cancelled</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="active_orders">
        <br>
        <!-- <input type="" name="order_stats" class="btn btn-danger" value="Cancel Order" />
        <input type="" name="order_stats" class="btn btn-success" value="Order Delievered" /> -->
        <?php 
            $i = 0;
            for($i=0;$i<=$_SESSION['num_rows_all'];$i++)     
            {
                if($_SESSION['all_orders'][$i]['status'] == 'active')
                {

                echo '<br><br>';
                ?>
                <form name="allorders_active" id="allorders_active" action="./db_store/admin_order_ops.php" method="post">
                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <p class="pull-right"><?php 
                        $timestamp = explode(" ",$_SESSION['all_orders'][$i]['timestamp']); 
                        $date = $timestamp[0];
                        $date_parts = explode("-", $date);
                        $date = $date_parts[2].'/'.$date_parts[1].'/'.$date_parts[0];
                        $time = $timestamp[1];
                        echo "placed on:   ".$date." ".$time;
                        ?></p>
                        <h3 class="panel-title"><?php echo $_SESSION['all_orders'][$i]['order_id']; ?></h3>
                        <input type="hidden" id="disp_order_id" name="disp_order_id" value="<?php echo $_SESSION['all_orders'][$i]['order_id']; ?>" />
                    </div>
                    <div class="panel-body">
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['customername']; ?></p>
                        <p>Customer Name</p>
                        
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['mobileno']; ?></p>
                        <p>Mobile #</p>

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['trainno']; ?></p>
                        <p>Train #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['coachno']; ?></p>
                        <p>Coach #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['birthno']; ?></p>
                        <p>Birth #</p>
                        
                    </div>
                    <div class="panel-footer">
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['price']; ?></p>
                        <p>Price</p>
                        <input type="submit" name="order_stats" class="btn btn-danger" value="CancelOrder" />
                        <input type="submit" name="order_stats" class="pull-right btn btn-success" value="OrderDelievered" />
                    </div>
                </div>
                </form>
                <?php
                //echo "<div class='panel panel-info'> <div class='panel-heading'>"."<h3 class='panel-title'>".$_SESSION['all_orders'][$i]['order_id']."</h3></div>"." <br><div class='panel-body'> "."<p class='pull-right'>".$_SESSION['all_orders'][$i]['customername']."</p><p>Customer Name</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['trainno']."</p><p>Train</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['coachno']."</p><p>Coach #</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['birthno']."</p><p>Birth #</p>"."</div> <div class='panel-footer'>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['price']."</p><p>Price</p>"."</div></div>";
                
                }
            }
        ?>

    </div>
    <div role="tabpanel" class="tab-pane" id="delivered">
        <?php 
            $i = 0;
            for($i=0;$i<=$_SESSION['num_rows_all'];$i++)     
            {
                if($_SESSION['all_orders'][$i]['status'] == 'delivered')
                {
                echo '<br><br>';
                ?>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <p class="pull-right"><?php 
                        $timestamp = explode(" ",$_SESSION['all_orders'][$i]['timestamp']); 
                        $date = $timestamp[0];
                        $date_parts = explode("-", $date);
                        $date = $date_parts[2].'/'.$date_parts[1].'/'.$date_parts[0];
                        $time = $timestamp[1];
                        echo "placed on:   ".$date." ".$time;
                        ?></p>
                        <h3 class="panel-title"><?php echo $_SESSION['all_orders'][$i]['order_id']; ?></h3>

                    </div>
                    <div class="panel-body">
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['customername']; ?></p>
                        <p>Customer Name</p>
                        
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['mobileno']; ?></p>
                        <p>Mobile #</p>

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['trainno']; ?></p>
                        <p>Train #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['coachno']; ?></p>
                        <p>Coach #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['birthno']; ?></p>
                        <p>Birth #</p>
                        
                    </div>
                    <div class="panel-footer">
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['price']; ?></p>
                        <p>Price</p>
                    </div>
                </div>
                <?php
                //echo "<div class='panel panel-success'> <div class='panel-heading'>"."<h3 class='panel-title'>".$_SESSION['all_orders'][$i]['order_id']."</h3></div>"." <br><div class='panel-body'> "."<p class='pull-right'>".$_SESSION['all_orders'][$i]['customername']."</p><p>Customer Name</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['trainno']."</p><p>Train</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['coachno']."</p><p>Coach #</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['birthno']."</p><p>Birth #</p>"."</div> <div class='panel-footer'>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['price']."</p><p>Price</p>"."</div></div>";
                }
            }
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="cancelled">
        <?php 
            $i = 0;
            for($i=0;$i<=$_SESSION['num_rows_all'];$i++)     
            {
                if($_SESSION['all_orders'][$i]['status'] == 'cancelled')
                {
                echo '<br><br>';
                ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <p class="pull-right"><?php 
                        $timestamp = explode(" ",$_SESSION['all_orders'][$i]['timestamp']); 
                        $date = $timestamp[0];
                        $date_parts = explode("-", $date);
                        $date = $date_parts[2].'/'.$date_parts[1].'/'.$date_parts[0];
                        $time = $timestamp[1];
                        echo "placed on:   ".$date." ".$time;
                        ?></p>
                        <h3 class="panel-title"><?php echo $_SESSION['all_orders'][$i]['order_id']; ?></h3>

                    </div>
                    <div class="panel-body">
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['customername']; ?></p>
                        <p>Customer Name</p>
                        
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['mobileno']; ?></p>
                        <p>Mobile #</p>

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['trainno']; ?></p>
                        <p>Train #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['coachno']; ?></p>
                        <p>Coach #</p>
                        

                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['birthno']; ?></p>
                        <p>Birth #</p>
                        
                    </div>
                    <div class="panel-footer">
                        <p class="pull-right"><?php echo $_SESSION['all_orders'][$i]['price']; ?></p>
                        <p>Price</p>
                    </div>
                </div>
                <?php
                //echo "<div class='panel panel-success'> <div class='panel-heading'>"."<h3 class='panel-title'>".$_SESSION['all_orders'][$i]['order_id']."</h3></div>"." <br><div class='panel-body'> "."<p class='pull-right'>".$_SESSION['all_orders'][$i]['customername']."</p><p>Customer Name</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['trainno']."</p><p>Train</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['coachno']."</p><p>Coach #</p>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['birthno']."</p><p>Birth #</p>"."</div> <div class='panel-footer'>"."<p class='pull-right'>".$_SESSION['all_orders'][$i]['price']."</p><p>Price</p>"."</div></div>";
                }
            }
        ?>
    </div>
    
  </div>

</div>

</div>
</div>
</div>
</div>

</div>
    <!-- /#wrapper -->
<?php endblock() ?>



<?php startblock('addtailscript') ?>


<?php endblock() ?>
</body>
</html>




