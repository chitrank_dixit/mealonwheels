<?php
require './components/core.inc.php';
session_unset();
session_destroy();
header('Location: index');
?>