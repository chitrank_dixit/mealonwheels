<?php 
require './components/ti.php';
require './components/core.inc.php';
?>
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<head>
<?php startblock('headscript') ?>
<link rel="stylesheet" href="./static/bootstrap/css/bootstrap.min.css"  />
<link href="./static/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
<?php endblock() ?>

<?php emptyblock('addheadscript') ?>

</head>

<body>
<!-- header -->
<?php startblock('header') ?>
    <div class="col-sm-6">
	<?php include 'components/signin_modal.html' ?>
    </div>
	<?php include 'components/header.php' ?>
<?php endblock() ?>

<!-- content here -->
<div id='main'>
  <?php emptyblock('main') ?>
</div>

<!-- footer -->
<?php startblock('footer') ?>
<?php include 'components/footer.php' ?>
<?php endblock() ?>

<?php startblock('tailscript') ?>
<script type="text/javascript" src="./static/bootstrap/js/jquery-2.1.1.js" > </script>
<script type="text/javascript" src="./static/bootstrap/js/bootstrap3.js" > </script>
<!-- <script type="text/javascript" src="./static/bootstrap/js/angular.js" > </script> -->
<script type="text/javascript" src="./static/bootstrap/js/validator.min.js" > </script>
<!-- <script type="text/javascript" src="./static/bootstrap/js/additional-methods.js" > </script> -->
<script type="text/javascript">
$(document).ready(function () {


$('#signin_form').validator('validate');
$('#signup_form').validator('validate');

</script>
<?php endblock() ?>

<?php emptyblock('addtailscript') ?>
</body>
</html>
