<?php require_once 'base.php' ?>
<html>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php startblock('addheadscript') ?>
    
    <link href="static/bootstrap/css/gsdk-base.css" rel="stylesheet" />  
    <style type="text/css">
    input[name=trainno]::-webkit-inner-spin-button, 
    input[name=trainno]::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
      margin: 0; 
    }

    input[name=birthno]::-webkit-inner-spin-button, 
    input[name=birthno]::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
      margin: 0; 
    }
    </style>
    
    
    <?php endblock() ?>
<head>
<title>Order Now | Mealsonwheels</title>
</head>
<body>

<?php startblock('main') ?>
<div class="image-container set-full-height" style="">
<!--   Big container   -->
    <div class="container">
        <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
           
            <!--      Wizard container        -->   
            <div class="wizard-container"> 
                <form name="order_form" id="order_form" data-toggle="validator" action="./db_store/order_db" method="POST">
                <div class="card wizard-card ct-wizard-azzure" id="wizard">
                
                <!--        You can switch "ct-wizard-azzure"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
                
                      <div class="wizard-header">
                          <h3>
                             <b><?php echo $_SESSION['username'] ?></b> YOUR ORDER <br>
                             <small>Please follow the below instructions to order your thali</small>
                          </h3>
                      </div>
                      <ul class="nav nav-pills">
                            <li><a href="#thalidetails" data-toggle="tab">Choose Your Thali</a></li>
                            <li><a href="#traindetails" data-toggle="tab">Your Train Details</a></li>
                            <li><a href="#orderdetails" data-toggle="tab">Confirm Order</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="thalidetails">
                              <div class="row">
                                  <div class="col-sm-12">
                                    <h3 class="info-text"> Choose Your Thali</h4>
                                  </div>
                                  <justify>
                                  <div class="col-sm-10">
                                  <div class="panel panel-info">
                                    <div class="col-sm-10">
                                    <div class="panel-heading">
                                      <div class="panel-title">
                                      <p class="pull-right">Quantity</p>
                                      <p>Item</p>
                                      </div>
                                    </div>
                                    </div>
                                    <div class="panel-body">
                                  <div class="col-sm-10">
                                    <input type="checkbox" id="deluxeip" name="deluxeip"> Deluxe Thali 
                                    <div class="pull-right">
                                      <input class="form-control" type="number" name="deluxeq" id="deluxeq" value="0" onkeyup="setPrice()" disabled>
                                    </div>
                                  </div>
                                  </div>

                                  <div class="panel-body">
                                  <div class="col-sm-10">
                                    <input type="checkbox" id="maharajaip" name="maharajaip"> Maharaja Thali
                                    <div class="pull-right">
                                      <input class="form-control" type="number" name="maharajaq" id="maharajaq" value="0" onkeyup="setPrice()" disabled>
                                    </div>
                                  </div>
                                  </div>  

                                  <div class="panel-body">
                                  <div class="col-sm-10">
                                    <input type="checkbox" id="specialmalwaip" name="specialmalwaip"> Special Malwa Thali
                                    <div class="pull-right">
                                      <input class="form-control" type="number" name="specialmalwaq" id="specialmalwaq" onkeyup="setPrice()" value="0" disabled>
                                    </div>
                                  </div>
                                  </div>  

                                  <div class="panel-body">
                                  <div class="col-sm-10">
                                    <input type="checkbox" id="nonvegip" name="nonvegip"> Non-Veg Thali
                                    <div class="pull-right">
                                      <input class="form-control" type="number" name="nonvegq" id="nonvegq" onkeyup="setPrice()" value="0" disabled>
                                    </div>
                                  </div>
                                  </div>

                                  <div class="panel-body">
                                  <div class="col-sm-10">
                                    <input type="checkbox" id="eggip" name="eggip"> Egg Thali
                                    <div class="pull-right">
                                      <input class="form-control" type="number" name="eggq" id="eggq" onkeyup="setPrice()" value="0" disabled>
                                    </div>
                                  </div>
                                  </div>



                                  </div>
                                  <div>
                                    <div class="panel-footer">
                                    <p id="price" name="price" class="pull-right"></p>
                                    Price: 
                                    </div>
                                    <!-- <input type="text" name="n1" id="n1" onkeyup="sync()">
                                    <input type="text" name="n2" id="n2"/> -->
                                  </div>

                                  </div>


                                  </justify>

                              </div>
                            </div>
                            
                            <div class="tab-pane" id="traindetails">
                                <i>you can find the following details on the ticket</i><br><br>
                                <div class="row">
                                                                    
                                  <div class="col-sm-10 form-group">
                                  <label>Name: </label><br>
                                  <input type="text" data-toggle="validator" name="customername" id="customername" class="form-control" data-error="please enter your name" required/>
                                  <div class="help-block with-errors"></div>
                                  </div>

                                  <div class="col-sm-10 form-group">
                                  <label>Mobile No: </label><br>
                                  +91 <input type="text" data-toggle="validator" name="mobileno" id="mobileno" class="form-control" data-error="please enter 10 digit mobile number" data-minlength="10" maxlength="10" required/>
                                  <div class="help-block with-errors"></div>
                                  </div>


                                  <div class="col-sm-10 form-group">
                                  <label>Train No: </label><br>
                                  <input type="number" data-toggle="validator" name="trainno" id="trainno" class="form-control" data-error="please enter the train number" required/>
                                  <div class="help-block with-errors"></div>
                                  </div>
                                  

                                  <div class="col-sm-10 form-group">
                                  <label>Coach No: </label><br>
                                  <input type="text" data-toggle="validator" name="coachno" id="coachno" class="form-control" data-error="please enter coach number" required/>
                                  <div class="help-block with-errors"></div>
                                  </div>

                                  <div class="col-sm-10 form-group">
                                  <label>Birth No: </label><br>
                                  <input type="number" data-toggle="validator" name="birthno" id="birthno" class="form-control" data-error="please enter birth number" required/>
                                  <div class="help-block with-errors"></div>
                                  </div>

                                  
                                </div>
                            </div>
                            <div class="tab-pane" id="orderdetails" >
                                <i>Currently we have Cash on Delivery option</i><br><br>
                                
                                <div class="panel panel-info">
                                  <div class="panel-heading">
                                    <h3 class="panel-title">Order Details</h3>
                                  </div>
                                  <div class="panel-body">
                                    <div class="col-sm-10">
                                    <label class="pull-right" id="name_o"></label>
                                    <label>Name :</label>
                                    </div>

                                    <div class="col-sm-10">
                                    <label class="pull-right" id="mobile_o"></label>
                                    <label>Mobile No :</label>
                                    </div>

                                    <div class="col-sm-10">
                                    <label class="pull-right" id="train_o"></label>
                                    <label>Train No :</label>
                                    </div>

                                    <div class="col-sm-10">
                                    <label class="pull-right" id="coach_o"></label>
                                    <label>Coach No :</label>
                                    </div>

                                    <div class="col-sm-10">
                                    <label class="pull-right" id="birth_o"></label>
                                    <label>Birth No :</label>
                                    </div>

                                    <div class="col-sm-10">
                                      <b>Items Ordered</b>
                                      <div id="deluxe_show">
                                        <p id="db" class="pull-right"></p>
                                        <p id="da" ></p>
                                      </div>

                                      <div id="maharaja_show">
                                        <p id="mb" class="pull-right"></p>
                                        <p id="ma"></p>
                                      </div>

                                      <div id="specialmalwa_show">
                                        <p id="sb" class="pull-right"></p>
                                        <p id="sa"></p>
                                      </div>

                                      <div id="nonveg_show">
                                        <p id="nb" class="pull-right"></p>
                                        <p id="na"></p>
                                      </div>

                                      <div id="egg_show">
                                        <p id="eb" class="pull-right"></p>
                                        <p id="ea" ></p>
                                      </div>

                                    </div>

                                    <div class="col-sm-10">
                                    <label class="pull-right" id="thali_quantity"></label>
                                    <label id="thali_ordered"></label>
                                    </div>
                                    

                                  </div>

                                  <div class="panel-footer">
                                    
                                    <p class="pull-right" id="order_price" name="order_price"></p>
                                    <label>Price</label>
                                    <input type="hidden" id="get_price" name="get_price" value="0" />
                                    
                                  </div>
                                  <p id="finish-error"></p>
                                </div>

                                
                            </div>
                        </div>
                        <div class="wizard-footer">
                              <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-info btn-wd btn-sm' name='next' value='Next' onclick="order_report()"/>
                                    <input type='submit' id="finish" class='btn btn-finish btn-fill btn-info btn-wd btn-sm' name='finish' onclick="finish_status()" value='Finish' disabled/>
                                    
        
                                </div>
                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                    
                                </div>
                                <div class="clearfix"></div>
                        </div>  
                </div>
                </form>
            </div> <!-- wizard container -->
        </div>
        </div> <!-- row -->
    </div> <!--  big container -->
</div>    





      


<?php endblock() ?>



<?php startblock('addtailscript') ?>
<script src="static/bootstrap/js/bootstrap-spinedit.js"></script>
<script src="static/bootstrap/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
  <script src="static/bootstrap/js/wizard.js"></script>
  <!-- <script src="static/bootstrap/js/vanilla.js"></script> -->

<script type="text/javascript">

// making the wizards tab clickable
$('#traindetails').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})


//form validator
$('#order_form').validator('validate');

// order detail variables

var deluxe_t,maharaja_t,specialmalwa_t,nonveg_t,egg_t,price;

// user detail variables

var name, customername,mobileno, trainno, coachno, birthno ;

$('#deluxeip').change( function() {
    var isChecked = this.checked;
    
    if(isChecked) {
        $("#deluxeq").attr("disabled", false);
        
    } else {
        $("#deluxeq").attr("disabled", true);
        //document.getElementById('deluxeq').value=0;
    }
    
});

$('#maharajaip').change( function() {
    var isChecked = this.checked;
    
    if(isChecked) {
        $("#maharajaq").attr("disabled", false);
        
    } else {
        $("#maharajaq").attr("disabled", true);
        // document.getElementById("maharajaq").value=0;
    }
    
});

$('#specialmalwaip').change( function() {
    var isChecked = this.checked;
    
    if(isChecked) {
        $("#specialmalwaq").attr("disabled", false);
        
    } else {
        $("#specialmalwaq").attr("disabled", true);
        // document.getElementById("specialmalwaq").value=0;
    }
    
});

$('#nonvegip').change( function() {
    var isChecked = this.checked;
    
    if(isChecked) {
        $("#nonvegq").attr("disabled", false);
        
    } else {
        $("#nonvegq").attr("disabled", true);
        // document.getElementById("nonvegq").value=0;
    }
    
});

$('#eggip').change( function() {
    var isChecked = this.checked;
    
    if(isChecked) {
        $("#eggq").attr("disabled", false);
        
    } else {
        $("#eggq").attr("disabled", true);
        // document.getElementById("eggq").value=0;
    }
    
});

/*$('#order_form').validate({
    rules: {
        
        
    },
    
    highlight: function (element) {
        $(element).closest('control-group').removeClass('success').addClass('error');
    },
    success: function (element) {
        element.text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
    }
});*/


delux_t = document.getElementById('deluxeq');
maharaja_t = document.getElementById('maharajaq');
specialmalwa_t = document.getElementById('specialmalwaq');
nonveg_t = document.getElementById('nonvegq');
egg_t = document.getElementById('eggq');
price = 0;
customername = document.getElementById('customername');
mobileno = document.getElementById('mobileno');
trainno = document.getElementById('trainno');
coachno = document.getElementById('coachno');
birthno = document.getElementById('birthno');


function setPrice()
{
  /*delux_t = document.getElementById('deluxeq');
  maharaja_t = document.getElementById('maharajaq');
  specialmalwa_t = document.getElementById('specialmalwaq');
  nonveg_t = document.getElementById('nonvegq');
  egg_t = document.getElementById('eggq');
*/

  /*cal_price = (delux_t * 80) + (maharaja_t * 100) + (specialmalwa_t * 120) + (nonveg_t * 120) + (egg_t * 100);*/

  price = (delux_t.value * 100 ) + (maharaja_t.value *120) + (specialmalwa_t.value *120) + (nonveg_t.value * 120) +  (egg_t.value *120);
  document.getElementById('price').innerHTML = price;
  document.getElementById('order_price').innerHTML = price;

}

function order_report()
{
  

  document.getElementById('name_o').innerHTML = customername.value;
  document.getElementById('mobile_o').innerHTML = mobileno.value;
  document.getElementById('train_o').innerHTML = trainno.value;
  document.getElementById('coach_o').innerHTML = coachno.value;
  document.getElementById('birth_o').innerHTML = birthno.value;
  if(delux_t.value >0)
  {
    document.getElementById('da').innerHTML = 'Deluxe Thali';
    document.getElementById('db').innerHTML = delux_t.value;
  }
  if(maharaja_t.value >0)
  {
    document.getElementById('ma').innerHTML = 'Maharaja Thali';
    document.getElementById('mb').innerHTML = maharaja_t.value;
  }
  if(specialmalwa_t.value >0)
  {
    document.getElementById('sa').innerHTML = 'Special Malwa Thali';
    document.getElementById('sb').innerHTML = specialmalwa_t.value;
  }
  if(nonveg_t.value >0)
  {
    document.getElementById('na').innerHTML = 'Non Veg Thali';
    document.getElementById('nb').innerHTML = nonveg_t.value;
  }
  if(egg_t.value >0)
  {
    document.getElementById('ea').innerHTML = 'Egg Thali';
    document.getElementById('eb').innerHTML = egg_t.value;
  }



  if((customername.value!='') && (mobileno.value!=0) && (trainno.value!='') && (coachno.value!='') && (birthno.value!='') &&  (price > 0) )
  {
    $("#finish").attr("disabled", false);

  }
  else
  {
    $("#finish").attr("disabled", true);
    //document.getElementById('finish-error').innerHTML= 'Order has missing details please recheck';
  }
  
}



function sync()
{
  var n1 = document.getElementById('n1');
  var n2 = document.getElementById('n2');
  var delux = document.getElementById('deluxeq');
  n2.value = (delux.value) * 2;
}





function finish_status(){
  /*if(((delux_t.value > 0) || (maharaja_t.value > 0) || (specialmalwa_t.value > 0) || (nonveg_t.value > 0)
  || (egg_t.value > 0) || ) && ((customername.value).length > 0) && (trainno.value).length > 0) && (coachno.value).length > 0) && (birthno.value).length > 0) )  )
  {
    $("#finish").attr("disabled", false);
  }*/
  $("#deluxeq").attr("disabled", false);
  $("#maharajaq").attr("disabled", false);
  $("#specialmalwaq").attr("disabled", false);
  $("#nonvegq").attr("disabled", false);
  $("#eggq").attr("disabled", false);
  var get_price = document.getElementById('get_price');
  get_price.value= price;
  // $.post( 'db_store/order_db.php', {final_price: price});

  
}



</script>
<?php endblock() ?>
</body>
</html>




